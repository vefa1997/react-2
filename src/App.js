import React, {Component} from 'react';
import CardExampleCard from "./component/CardExampleCard";

class App extends Component {
    render() {
        return (
            <div>
                <CardExampleCard/>
            </div>
        );
    }
}

export default App;